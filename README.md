# README.md - DOCKER #

## Uygulama Gerçekleştirimi ##
Bu işlem için iki Docker image kullanıldı. İlki JAVA uygulaması ikincisi ise PostgreSQL için.

Java uygulaması bir blog için gerekli CRUD işlemlerini gerçekleştiriyor.

Uygulamanın Controller ve Service dosyaları aşağıdaki gibidir.
```java
public class BlogService {
    BlogRepository blogRepository;

    public BlogService(BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    public void addBlog(Blog blog) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(blog.getTitle());
        if (blogOptional.isPresent()) throw new IllegalStateException("blog title in use");
        blogRepository.save(blog);
    }

    public void deleteBlog(String title) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(title);
        if (blogOptional.isEmpty()) throw new IllegalStateException("No blog with title "+title);
        blogRepository.deleteById(blogOptional.get().getId());
    }

    public List<Blog> getBlogs() {
        return blogRepository.findAll();
    }

    @Transactional
    public void updateBlog(String title, String newBody) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(title);
        if (blogOptional.isEmpty()) throw new IllegalStateException("No blog with title "+title);
        blogOptional.get().setBody(newBody);
    }
}
```

```java
@RestController
@RequestMapping(path = "/")
public class BlogController {
    @Autowired
    BlogService blogService;

    @PostMapping("/blogs")
    public void addBlog(@RequestBody Blog blog) {
        blogService.addBlog(blog);
    }

    @DeleteMapping(path="/blogs/{title}")
    public void deleteBlog(@PathVariable("title") String title) {
        blogService.deleteBlog(title);
    }

    @GetMapping("/blogs")
    public List<Blog> getBlogs() {
        return blogService.getBlogs();
    }

    @PutMapping("/blogs/{title}")
    public void updateBody(@PathVariable("title") String title, @RequestParam String newBody) {
        blogService.updateBlog(title,newBody);
    }

}
```



## Uygulama için Image Yaratımı ##
Gerçekleştirimden sonra, bu uygulamayı bir docker image haline getirdim.Bunun için aşağdaki
Dockerfile kullanıldı.
```sh
FROM adoptopenjdk/openjdk11:alpine-jre
ADD target/dojo-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
```
Burda kullanılan "dojo-0.0.1-SNAPSHOT.jar" dosyası bu işlemden önce aşağıdaki komut kullanılarak oluşturuldu
```sh
mvn clean install -DskipTests=true
```


## Docker Compose ile Bağlantı ##
Uygulama için bir image yarattıktan sonra, PostgreSQL için bir image olşuturmak ve bu iki image arasında
bağlantı kurmak için aşağıda gösterilen "docker-compose.yml" dosyasını kullandım.
```java
version: '3.1'
services:
  app:
    container_name: springboot-postgresql
    image: springboot-postgresql
    build: ./
    ports:
      - "8080:8080"
    depends_on:
      - postgresqldb
  postgresqldb:
    image: postgres
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_PASSWORD=password
      - POSTGRES_USER=mebas
      - POSTGRES_DB=dojo
```

Bu aşamadan sonra iki image yaratılmış ve birbirlerine bağlanmış oldu.

## Çalıştırma ##

Image yaratımı bittikten sonra, aşağıdaki komut ile uygulama ve veri tabanını ayağa kaldırdım.

```sh
    docker-compose up
```

Uygulamanın düzgün çalışıp çalışmadığını kontrol etmek için implementasyonda kullandığım
IntellijIDEA'nın HTTP Clint özelliğini kullanarak çeşitli requestlerde bulundum,
"generated-request.http" dosyası ile yapılan bu işlemin içeriği aşağıdaki gibidir.

```java
    POST http://localhost:8080/blogs
    Content-Type: application/json
    
    {
      "title": "title",
      "author": "Emre",
      "body": "body"
    
    }
    
    
    ###
    GET http://localhost:8080/blogs
    
    
    ###
    DELETE http://localhost:8080/blogs/title
    ###
    PUT http://localhost:8080/blogs/title?newBody=newBodyContent
    ###
    

```
Bu requestler sonucunda beklenen çıktılar elde edilmiştir.
