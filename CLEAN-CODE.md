### TEMİZ KOD ###
		Temiz kodlama hakkında notlar genel başlıklar halinde aşağıdaki gibidir.
		
##	Konseptin Genel Gereksinimleri ##
	
		Herhangi bir kodlama elemanında tekrarlamalar yapılmamalı, olabildiğince kapsamlı bir tasarım yapılmalıdır.
		Kod baştan aşağı bir anlam içermeli ve aynı bağlamdaki elemanlar alakalı yerlerde kullanılmalıdır.
		Modüller arası ilişkiyi en aza indirip, olabildiğince esnek ve bağımsız bir yapı yaratılmalıdır.
	
##	İsimlendirme ##
		
		Değişken : Ne hakkında bilgi taşıdığı net anlaşılmalıdır
			
		Sınıf : Sorumlulukları kapsayan isimler olmalıdır, fiil içermemelidir.
		
		Method: Ne iş yaptığını "Tek Görevlilik" çerçevesinde anlatmalıdır.
		
		Boolean : Cevabının değişkenler ile alakalı olması gerekli, bir durumun kontrolünü açıkça belirtebilmelidir. 
		          Binary sorulara cevap verebilmelidir.
		
		İsimlendirmeleri anlık değil, genel kapsamda yapıp kodu anlaşılır yapmalı, yapılmak isteneni en uygun
		şekilde ifade edecek şekilde tasarlanmalıdır. Kısaltma kullanmak, bizim için anlaşılır olsa da başkası için
		olamayabileceği için, pek iyi değildir.
	
##	Koşullar ##
		Kontrol edilen durumun ne olduğunu en iyi anlatacak şekilde tasarlanmalı, koşul kısmını olabildiğince
		kısa tutmalı, gerekirse başka bir değişken kullanarak enkapsüle etmeli.
	
##	Fonksiyonlar ##
		Tekrardan kaçınmak temel amacıyla küçük ve tek amaca hizmet eden kod blokları kullanılmalı ve verilen 
		parametreler ve sayıları da bu tasarımı desteklemeli.
	
##	Exception Handling ##
		İşlerin ters gitmesi durumuna karşılık önlem alınmalı ve istenmeyen durumlarla karşılaşıldığı zaman
		uygun bilgilendirme ile birlikte gerekli aksiyonlar gerçekleştirilmeli.
	
##	Sınıflar ##
		Olabildiğince küçük ve kapsamı net olarak belli olmalı. Koşullara göre kapsamı genişleyen yapılar olmamalı.
		İyi organize edilmiş, diğer modüllerle gereğinden fazla alaka  kurmayan sınıflar gelecek değişimlerde kolaylık
		sağlar.
		
##	Yorum Satırları ##
		Yorum satırları direk olarak kodu açıklamak için değil, kodun gerçek manada kendini anlatmaya yetersiz 
		kaldığı durumlarda kullanılmalıdır. Her zaman ilk öncelik yorum satırı yerine ne yaptığını net bir şekilde
		açıklayan kod blokları olmalıdır, bunun mümkün olmadığı yerlerde yorum satırları kullanılmalıdır.
		
##	Biçim ##
		Baştan aşağı kodu okurken bir örgü içerisinde olmalı, alakalı değişkenler ve methodlar bir arada olmalı.
	
##	Güvenlik ##
		Kullandığımız dilin bize verdiği imkanlar doğrultusunda güvenli bir kod yazmalıyız, kod elemanlarının 
		kullanılması gereken kapsamlar dışında kullanılmamasını sağlamalıyız. Bu hem güvenliğin yanında esnekliği, 			
		bakım yapılabilirliği, anlaşılabilirliği ve test edilebilirliği konusunda da yardımcı olur. 
		Örneğin Javada kullanılan private ve public anahtar kelimeleri. 
		
##	Testler ##
		Testler bir kodun güvenilirliğini ve bakım-edilebilirliği konusunda önemlidir. Okunabilir kodlar yazmak
		kadar okunabilir testler yazmak da önemlidir. Kapsamı belli, bağımsız ve hızlı testler kullanılmalıdır.
