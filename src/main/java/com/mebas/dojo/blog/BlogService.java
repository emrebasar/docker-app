package com.mebas.dojo.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class BlogService {
    BlogRepository blogRepository;

    public BlogService(BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    public void addBlog(Blog blog) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(blog.getTitle());
        if (blogOptional.isPresent()) throw new IllegalStateException("blog title in use");
        blogRepository.save(blog);
    }

    public void deleteBlog(String title) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(title);
        if (blogOptional.isEmpty()) throw new IllegalStateException("No blog with title "+title);
        blogRepository.deleteById(blogOptional.get().getId());
    }

    public List<Blog> getBlogs() {
        return blogRepository.findAll();
    }

    @Transactional
    public void updateBlog(String title, String newBody) {
        Optional<Blog> blogOptional = blogRepository.findBlogByTitle(title);
        if (blogOptional.isEmpty()) throw new IllegalStateException("No blog with title "+title);
        blogOptional.get().setBody(newBody);
    }
}
