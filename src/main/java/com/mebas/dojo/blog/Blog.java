package com.mebas.dojo.blog;

import javax.persistence.*;

@Entity
public class Blog {
    @Id
    @SequenceGenerator(name = "dojo_sequence", sequenceName = "dojo_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dojo_sequence")
    private long id;

    private String title;

    private String author;

    private String body;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
