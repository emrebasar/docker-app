package com.mebas.dojo.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/")
public class BlogController {
    @Autowired
    BlogService blogService;

    @PostMapping("/blogs")
    public void addBlog(@RequestBody Blog blog) {
        blogService.addBlog(blog);
    }

    @DeleteMapping(path="/blogs/{title}")
    public void deleteBlog(@PathVariable("title") String title) {
        blogService.deleteBlog(title);
    }

    @GetMapping("/blogs")
    public List<Blog> getBlogs() {
        return blogService.getBlogs();
    }

    @PutMapping("/blogs/{title}")
    public void updateBody(@PathVariable("title") String title, @RequestParam String newBody) {
        blogService.updateBlog(title,newBody);
    }

}
